class table(object):
    __instance = None

    def __new__(self):
        if table.__instance is None:
            table.__instance = object.__new__(self)

        else:
            print("object already created")
            print(table.__instance)
        return table.__instance

    def __init__(self):
        self.mysql_conn = None
        self.batch_id = None
        self.raw_table_absolute_path = None
        self.history_table_absolute_path = None
        self.history_table_name = None
        self.raw_table_name = None
        self.is_incremental = None
        self.last_modified_timestamp_column = None
        self.unqiue_identfier = None
        self.created_at_timestamp_column = None
        self.sink_db_name = None
        self.raw_db_name = None
        self.temp_path = None
        self.table_name = None
        self.backup_location = None
        self.read_path_name = None

    @property
    def batch_id(self):
        return self.__batch_id

    @batch_id.setter
    def batch_id(self, value):
        self.__batch_id = value

    @property
    def raw_table_absolute_path(self):
        return self.__raw_table_absolute_path

    @raw_table_absolute_path.setter
    def raw_table_absolute_path(self, value):
        self.__raw_table_absolute_path = value

    @property
    def history_table_absolute_path(self):
        return self.__history_table_absolute_path

    @history_table_absolute_path.setter
    def history_table_absolute_path(self, value):
        self.__history_table_absolute_path = value

    @property
    def is_incremental(self):
        return self.__is_incremental

    @is_incremental.setter
    def is_incremental(self, value):
        self.__is_incremental = value

    @property
    def raw_table_name(self):
        return self.__raw_table_name

    @raw_table_name.setter
    def raw_table_name(self, value):
        self.__raw_table_name = value

    @property
    def history_table_name(self):
        return self.__history_table_name

    @history_table_name.setter
    def history_table_name(self, value):
        self.__history_table_name = value

    @property
    def last_modified_timestamp_column(self):
        return self.__last_modified_timestamp_column

    @last_modified_timestamp_column.setter
    def last_modified_timestamp_column(self, value):
        self.__last_modified_timestamp_column = value

    @property
    def unqiue_identfier(self):
        return self.__unqiue_identfier

    @unqiue_identfier.setter
    def unqiue_identfier(self, value):
        self.__unqiue_identfier = value

    @property
    def is_active(self):
        return self.__is_active

    @is_active.setter
    def is_active(self, value):
        self.__is_active = value

    @property
    def created_at_timestamp_column(self):
        return self.__created_at_timestamp_column

    @created_at_timestamp_column.setter
    def created_at_timestamp_column(self, value):
        self.__created_at_timestamp_column = value

    @property
    def sink_db_name(self):
        return self.__sink_db_name

    @sink_db_name.setter
    def sink_db_name(self, value):
        self.__sink_db_name = value

    @property
    def temp_path(self):
        return self.__temp_path

    @temp_path.setter
    def temp_path(self, value):
        self.__temp_path = value

    @property
    def table_name(self):
        return self.__table_name

    @table_name.setter
    def table_name(self, value):
        self.__table_name = value

    @property
    def mysql_conn(self):
        return self.__mysql_conn

    @mysql_conn.setter
    def mysql_conn(self, value):
        self.__mysql_conn = value

    @property
    def raw_db_name(self):
        return self.__raw_db_name

    @mysql_conn.setter
    def raw_db_name(self, value):
        self.__raw_db_name = value

    @property
    def backup_location(self):
        return self.__backup_location

    @backup_location.setter
    def backup_location(self, value):
        self.__backup_location = value

    @property
    def read_path_name(self):
        return self.__read_path_name

    @read_path_name.setter
    def read_path_name(self, value):
        self.__read_path_name = value
