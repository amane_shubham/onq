from paramiko.sftp_client import SFTP
from paramiko.sftp_client import SFTPClient
import paramiko
import re

path = "/efs/pulse-home-3/hilton-ftp-backup"
key = "/Users/shubhamamane/Documents/SFTP_DATA.pem"

class SftpConnect(object):
    def __init__(self):
        self.sftp = None

    def connect_to_sftp(self, sftp_host, sftp_port, sftp_username,  sftp_key):
        # initiate SFTP connection
        try:
            transport = paramiko.Transport((sftp_host, sftp_port))
            key = paramiko.RSAKey.from_private_key_file(sftp_key)
            print("connecting")
            transport.connect(None, sftp_username, pkey=key)
            print("connected")
            # Go!
            self.sftp = paramiko.SFTPClient.from_transport(transport)
        except Exception as e:
            print("Error : " + e)
        return self.sftp

    def sftp_get_file(self, local_directory, remote_directory, file):
        # pull file from remote directory and send to local directory
        try:
            self.sftp.get(file in remote_directory, file in local_directory)
        except Exception as e:
            print("Error : " + e)

    def sftp_change_dir(self, path):
        path = self.sftp.chdir(path)
        return path

    def sftp_list_dir(self, path):
        list_of_dir = self.sftp.chdir(path)
        return list_of_dir

    def sftp_get_dir_name(self, data):
        try:
            location_name = data.split("_")[1].split(".")[0]
            result = " ".join(re.findall("[a-zA-Z]+", location_name))
            print(result)
        except Exception as e:
            print("Error : " + e)
        return result