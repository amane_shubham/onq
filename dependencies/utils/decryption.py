import os
from subprocess import Popen, PIPE, STDOUT

def decrypt(encrypted_user, encrypted_pass, encrypted_key):
    path = os.path.dirname(os.path.realpath(__file__))
    application_jar = path + "/cryptography-utility.jar"
    class_name = "com.fpg.security.cryptography.Decryption"
    encrypted_username = encrypted_user
    encrypted_password = encrypted_pass
    encrypted_hashed_key = encrypted_key
    process = Popen(
        [
            "java",
            "-cp",
            application_jar,
            class_name,
            encrypted_username,
            encrypted_password,
            encrypted_hashed_key,
        ],
        stdout=PIPE,
        stderr=STDOUT,
    )
    decrypted_cred = None
    for line in process.stdout:
        decrypted_cred = line.decode("utf-8").strip().split(",")
    return (str(decrypted_cred[0]), str(decrypted_cred[1]))