from email.mime.multipart import MIMEMultipart
from dynaconf import settings
from email.mime.application import MIMEApplication
import smtplib
from email.mime.text import MIMEText
from email import encoders
from email.mime.base import MIMEBase
import pandas as pd
pd.set_option('display.max_colwidth', -1)
from dependencies.utils.decryption import decrypt
import os

(status_sender, status_sender_password) = decrypt(settings.STATUS_SENDER, \
                                                                 settings.STATUS_SENDER_PASSWORD, \
                                                                 settings.STATUS_SENDER_KEY)
env_stream = os.popen('echo $ENV_FOR_DYNACONF')
envi = env_stream.read()
env = envi.strip('\n')

def send_status_notification(system,df,attachments=[]):
    outer = MIMEMultipart()
    outer['Subject'] = '{} data uploaded to the raw layer ({})'.format(system,env)
    outer['To'] = settings.STATUS_EMAIL_RECIPIENTS
    outer['From'] = status_sender
    outer.preamble = 'You will not see this in a MIME-aware mail reader.\n'
    for f in attachments:
        with open(f, 'rb') as fp:
            body = fp.read()
            msg = MIMEBase('application', "octet-stream")
            msg.set_payload(body)
        encoders.encode_base64(msg)
        msg.add_header('Content-Disposition', 'attachment', filename=os.path.basename(f))
        outer.attach(msg)
    mail_data = """<html>
<body>
Hi team,

<br> <br> Please find below the {} reports which were uploaded to the raw layer:
<br> 
{}

<br> Thank you,
<br> Dev team

</body>
</html>""".format(system, df.to_html(index=False))

    outer.attach(MIMEText(mail_data.replace('<table>','<table border="1">'), 'html'))
    composed = outer.as_string()
    try:
        send_email_notification(composed)
    except Exception as e:
        raise("Unable to send the email. Error: {}".format(str(e)))


def send_email_notification(composed):
    with smtplib.SMTP(settings.STATUS_SMTP_HOST, settings.STATUS_SMTP_PORT) as s:
        s.ehlo()
        s.starttls()
        s.ehlo()
        s.login(status_sender, status_sender_password)
        s.sendmail(status_sender, settings.STATUS_EMAIL_RECIPIENTS.split(','), composed)
        s.close()
        print("Notification Email sent!")
