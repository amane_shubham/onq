import logging
import logging.handlers
import boto3
import atexit
import os
import sys
from datetime import datetime
from dynaconf import settings


"""
logging
~~~~~~~

This module contains a class that wraps the log4j object instantiated
by the active SparkContext, enabling Log4j logging for PySpark using.
"""


class Log4j(object):
    """Wrapper class for Log4j JVM object.

    :param spark: SparkSession object.
    """

    def __init__(self, spark):
        # get spark app details with which to prefix all messages
        conf = spark.sparkContext.getConf()
        app_id = conf.get('spark.app.id')
        app_name = conf.get('spark.app.name')

        log4j = spark._jvm.org.apache.log4j
        message_prefix = '<' + app_name + ' ' + app_id + '>'
        self.logger = log4j.LogManager.getLogger(message_prefix)

    def error(self, message):
        """Log an error.

        :param: Error message to write to log
        :return: None
        """
        self.logger.error(message)
        return None

    def warn(self, message):
        """Log an warning.

        :param: Error message to write to log
        :return: None
        """
        self.logger.warn(message)
        return None

    def info(self, message):
        """Log information.

        :param: Information message to write to log
        :return: None
        """
        self.logger.info(message)
        return None


# Logging Levels
# https://docs.python.org/3/library/logging.html#logging-levels
# CRITICAL  50
# ERROR 40
# WARNING   30
# INFO  20
# DEBUG 10
# NOTSET    0

levels = {"DEBUG": logging.DEBUG,
          "INFO": logging.INFO,
          "ERROR": logging.ERROR,
          "WARNING": logging.WARNING,
          "CRITICAL": logging.CRITICAL
          }


current_time = datetime.now()
current_date = current_time.strftime("%Y-%m-%d")

def get_logger(file_name = current_date, logger_name=settings.LOGGER_NAME):
    log_file = get_log_file(file_name)
    with open(log_file, 'a+'):
        pass

    # # To store in file
    logging.basicConfig(format=settings.LOG_FORMAT, filemode='a+',
                        filename=log_file,
                        level=levels[settings.get("LOG_LEVEL")])

    logger = logging.getLogger(logger_name)

    #
    # atexit.register(write_logs, file=log_file)
    return logger

def get_log_file(file_name = current_date):
    file_path = sys.modules[__name__].__file__
    project_path = os.path.dirname(os.path.dirname(file_path))
    log_location = project_path + '/logs/'
    if not os.path.exists(log_location):
        os.makedirs(log_location)
    log_file = log_location + str(file_name) + '.log'
    return log_file