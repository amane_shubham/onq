"""

"""

from pyspark.sql import HiveContext
from pyspark.sql.functions import col, lit
from pyspark.sql.functions import concat_ws
from pyspark.sql.types import StructType, ArrayType
import datetime
import functools
from dependencies.utils.logger import get_logger
from table import table

logging = get_logger()


def get_city_hash(all_string):
    return CityHash64(all_string)


class delta_connector:
    def __init__(self, spark, client):
        self.spark = spark
        self.client = client
        self.sqlContext = HiveContext(spark)

    def custom_schema(self, schema_array, actual_df, table_name, request_column = None):
        # schema_array = schema_df.groupBy(schema_df.ing_tablename, schema_df.field, schema_df.type,
        #                                  schema_df.hive_column_name, schema_df.hive_datatype).count().orderBy(
        #     schema_df.ing_tablename, schema_df.field, schema_df.type).collect()

        for py in schema_array:
            if py['ONQ_RAW_Location_Metrics'] == 'Y' and table_name == 'ONQ_RAW_Location_Metrics':
                # for py_array in py
                for col_array in actual_df.columns:
                    actual_df = actual_df.withColumn(str(col_array),
                                                     actual_df[str(col_array)].cast("string"))

                    actual_df = actual_df.withColumn(py[str('data_point_name_in_Datalake')], actual_df[str(col_array)].cast("string"))#.cast(py['hive_datatype']))
            elif py['ONQ_RAW_Agent_Metrics'] == 'Y' and table_name == 'ONQ_RAW_Agent_Metrics':
                for col_array in actual_df.columns:
                    actual_df = actual_df.withColumn(py[str('data_point_name_in_Datalake')], actual_df[str(col_array)].cast("string"))#.cast(py['hive_datatype']))
            elif py['ONQ_RAW_Product_Metrics'] == 'Y' and table_name == 'ONQ_RAW_Product_Metrics':
                for col_array in actual_df.columns:
                    actual_df = actual_df.withColumn(py[str('data_point_name_in_Datalake')], actual_df[str(col_array)].cast("string"))#.cast(py['hive_datatype']))


        return actual_df

    def add_column_not_exist(self, df, column_list):
        for i in column_list:
            if i not in df.columns and i !='special_request_text' and i!='special_request_desc':
                print("Column not presented, ", i)
                df = df.withColumn(i.strip(), lit("NULL").cast("string"))

        return df

    def read_excel(self, spark, path):
        schema_df = spark.read.format("com.crealytics.spark.excel") \
            .option("useHeader", "true") \
            .option("dataAddress", "A1") \
            .option("treatEmptyValuesAsNulls", "true") \
            .option("inferSchema", "true") \
            .option("addColorColumns", "False") \
            .option("maxRowsInMey", 2000) \
            .load("file:///" + path)
        return schema_df

    def merge_columns(self, df1, df2):
        cols1 = df1.columns
        cols2 = df2.columns
        total_cols = sorted(cols1 + list(set(cols2) - set(cols1)))

        def expr(mycols, allcols):
            def processCols(colname):
                if colname in mycols:
                    return colname
                else:
                    return lit(None).alias(colname)

            cols = map(processCols, allcols)
            return list(cols)

        return expr(cols1, total_cols), expr(cols2, total_cols)

    def is_schema_same(self, df1, df2):

        df1_cols = [element.lower() for element in df1.columns]
        df2_cols = [element.lower() for element in df2.columns]

        missing_in_df2 = [item for item in df1_cols if item not in df2_cols]

        missing_in_df1 = [item for item in df2_cols if item not in df1_cols]
        if len(missing_in_df2) == 0 and len(missing_in_df1) == 0:
            return True

        return False

    def schema_union_between_dataframes(self, sqlContext, df_new, df_raw):
        raw_columns = df_raw.columns
        new_columns = df_new.columns

        missing_in_raw = [item for item in new_columns if item not in raw_columns]
        missing_in_new = [item for item in raw_columns if item not in new_columns]

        # construct  columns  for the new datafrae
        new_columns_for_new_dataframe = []
        for element in raw_columns:
            if element in missing_in_new:
                element = "NULL as `" + str(element) + "`"

                new_columns_for_new_dataframe.append(element)
            else:

                new_columns_for_new_dataframe.append("`" + element + "`")

        missing_in_raw_as_null = [','.join(["NULL as `" + str(elem) + "`" for elem in missing_in_raw])]

        if (len(missing_in_raw_as_null) == 1 and missing_in_raw_as_null[0] == ''):
            missing_in_raw_as_null = []

        columns_list_raw = ["`" + element + "`" for element in raw_columns] + missing_in_raw_as_null

        columns_list_raw_string = ' , '.join([str(elem) for elem in columns_list_raw])

        new_columns_list_string = ' , '.join([str(elem) for elem in new_columns_for_new_dataframe + missing_in_raw])

        df_new.createOrReplaceTempView("new_df")

        df_raw.createOrReplaceTempView("raw_df")

        new_df = sqlContext.sql(
            "select {columns_new} from new_df union  select {columns_raw} from raw_df where 1=0 ".format(
                columns_new=new_columns_list_string, columns_raw=columns_list_raw_string))
        # raw_df = sqlContext.sql("select {columns_raw} from raw_df ".format(columns_raw=columns_list_raw_string)
        raw_df = sqlContext.sql(
            "select {columns_new} from new_df where 1=0 union  select {columns_raw} from raw_df  ".format(
                columns_new=new_columns_list_string, columns_raw=columns_list_raw_string))
        return new_df, raw_df

    def append_new_chamges_to_history(self, output_df):

        history_path = table.history_table_absolute_path

        self.client.write_dataframe_as_append(output_df, history_path)

    def overwrite_raw_layer(self, output_df):

        database_name = table.sink_db_name
        raw_tablename = table.raw_table_name
        raw_table_absolute_path = table.raw_table_absolute_path

        self.client.write_dataframe_as_table_overwrite(self.spark, output_df, database_name, raw_tablename,
                                                       raw_table_absolute_path)

    def calulate_diff_in_two_dataframes(self, df1, df2, new_schema_column_list=None):
        op = df1.subtract(df2)

        print("show op of df2 - df1", df2.subtract(df1).count())

        if new_schema_column_list != None:
            return op.select(new_schema_column_list)

        return op

    def change_table_schema_with_new_dataframe_schema(self, df_history, sqlContext):

        database_name = table.sink_db_name
        tablename = table.history_table_name
        history_path = table.history_table_absolute_path
        temp_path = table.temp_path

        df_history.withColumn("batch_id", lit(table.batch_id)).limit(0).write.mode('overwrite').option("path",
                                                                                                       temp_path).format(
            "parquet").saveAsTable(database_name + "." + "tmp_" + tablename)

        sqlContext.sql("alter table {db}.{table} set location '{path}'".format(db=database_name,
                                                                               table="tmp_" + tablename,
                                                                               path=history_path))
        sqlContext.sql("drop table  {db}.{table}".format(db=database_name, table=tablename))
        sqlContext.sql("ALTER TABLE {db}.{temp_table} RENAME TO {db}.{histable}".format(db=database_name,
                                                                                        temp_table="tmp_" + tablename,
                                                                                        histable=tablename))
        return None

    def insert_incremental_data(self, new_dataframe, raw_table_dataframe, ):

        last_modified_time_column = table.last_modified_timestamp_column
        raw_table_name = table.raw_table_name
        table_raw_directory = table.raw_table_absolute_path
        table_history_directory = table.history_table_absolute_path
        unqiue_identfier = table.unqiue_identfier
        temp_path = table.temp_path
        sink_db_name = table.sink_db_name

        self.client.write_dataframe_as_append(new_dataframe, table_history_directory)

        union_df = new_dataframe.unionByName(raw_table_dataframe)
        unique_records = union_df.orderBy(col(last_modified_time_column).desc()).dropDuplicates(
            unqiue_identfier)

        self.client.write_dataframe_as_directory_overwrite(unique_records, temp_path)
        unique_records = self.client._read_dataframe_with_merge(self.spark, temp_path)

        self.client.write_dataframe_as_table_overwrite(self.spark, unique_records, sink_db_name, raw_table_name,
                                                       table_raw_directory)

        return None

    def insert_data_on_scehma_change(self, new_dataframe, raw_table_dataframe):

        last_modified_time_column = table.last_modified_timestamp_column
        raw_table_name = table.raw_table_name
        table_raw_directory = table.raw_table_absolute_path
        history_table_name = table.history_table_name
        table_history_directory = table.history_table_absolute_path
        temp_path = table.temp_path
        sink_db_name = table.sink_db_name

        new_df_schema = new_dataframe.schema
        new_df_schema_columns_list = new_dataframe.columns
        new_dataframe_copy = new_dataframe

        new_dataframe, raw_table_dataframe = self.schema_union_between_dataframes(self.sqlContext, new_dataframe,
                                                                                  raw_table_dataframe)
        new_changes = self.calulate_diff_in_two_dataframes(new_dataframe, raw_table_dataframe,
                                                           new_df_schema_columns_list)
        new_changes = new_changes.select(new_df_schema_columns_list)

        new_changes = self.spark.createDataFrame(new_changes.rdd, schema=new_df_schema)
        print("Scheam change found in {}".format(raw_table_name))


        # self.client.write_dataframe_as_table_overwrite(self.spark, new_dataframe_copy, sink_db_name, raw_table_name,
                                                       # table_raw_directory)
        self.client.write_dataframe_as_append(new_changes, table_raw_directory)
        # history_df = self.client._read_dataframe_with_merge(self.spark, table_history_directory)
        # self.change_table_schema_with_new_dataframe_schema(history_df, self.sqlContext)

    def insert_full_load_data(self, new_dataframe, raw_table_dataframe):

        table_raw_directory = table.raw_table_absolute_path
        table_history_directory = table.history_table_absolute_path

        # new_changes = self.calulate_diff_in_two_dataframes(new_dataframe, raw_table_dataframe)

        # print("New changes for full load count :", new_changes.count())

        self.client.write_dataframe_as_append(new_dataframe, table_raw_directory)
        # self.client.write_dataframe_as_directory_overwrite(new_dataframe, table_raw_directory)

    def insert_first_load(self, new_dataframe):

        raw_table_name = table.raw_table_name
        table_raw_directory = table.raw_table_absolute_path
        history_table_name = table.history_table_name
        table_history_directory = table.history_table_absolute_path
        sink_db_name = table.sink_db_name
        print("First  load for {}".format(raw_table_name))
        # self.client.write_dataframe_as_table_overwrite(self.spark, new_dataframe, sink_db_name, history_table_name,
        #                                                table_history_directory)
        self.client.write_dataframe_as_table_overwrite(self.spark, new_dataframe, sink_db_name, raw_table_name,
                                                       table_raw_directory)

    def _onClause(self, keyCols=[]):
        on_clause = " ".join(
            ['t.' + column + '=s.' + column + ' AND' for column in keyCols
             ]).rstrip('AND')
        return on_clause

    def _replace_special_characters(self, df):
        for column in df.columns:
            df = df.withColumnRenamed(column, column.replace(" ", "_") \
                                      .replace(".", "_") \
                                      .replace(";", "_") \
                                      .replace(",", "_") \
                                      .replace("-", "_") \
                                      .replace("&", "_") \
                                      .replace("/", "_") \
                                      .replace("(", "_") \
                                      .replace(")", "_"))
        return df

    def _aliasing(self, df, schema):
        replacements = {c: c.replace('.', '__') for c in schema if '.' in c}
        df = df.select([col(c).alias(replacements.get(c, c)) for c in schema])
        return df

    def _cast_array(self, df, schema):
        for col_name, dtype in schema:
            if dtype.startswith('array'):
                df = df.withColumn(col_name, df[col_name][0].cast('string'))

        return df

    def _flatten(self, schema, prefix=None):
        fields = []
        for field in schema.fields:
            name = prefix + '.' + field.name if prefix else field.name
            dtype = field.dataType
            if isinstance(dtype, ArrayType):
                dtype = dtype.elementType

            if isinstance(dtype, StructType):
                fields += self._flatten(dtype, prefix=name)
            else:
                fields.append(name)

        return fields

    def _rename_duplicate_columns(self, df):
        columns = df.columns
        duplicate_column_indices = list(set([columns.index(col) for col in columns if columns.count(col) == 2]))
        for index in duplicate_column_indices:
            columns[index] = columns[index] + '2'
        df = df.toDF(*columns)
        return df

    def _hash_delta(self, df):
        df = df.withColumn("all_concat", concat_ws("||", *df.columns))
        df = df.withColumn("orderInformationRecordId", get_city_hash("all_concat"))
        # df = df.withColumn("orderInformationRecordId", sha2("all_concat", 256))
        df = df.drop("all_concat")
        return df

    def _delta_transform(self, df):

        df = self._rename_duplicate_columns(df)
        df = self._replace_special_characters(df)
        fields = self._flatten(df.schema)
        df = self._aliasing(df, fields)
        df = self._cast_array(df, df.dtypes)
        df = self._replace_special_characters(df)
        # df = self._hash_delta(df)
        return df

    def audit_log(self, batch_id, process_type, sp_name, database_name,
                  table_names, input_output):
        try:
            hiveContext = self.sqlContext.get_hive_context()
            for table_name, table in table_names.items():
                audit_table = 'audit_log'
                print(table_name)
                row_count = table.count()
                sql_stmt = "insert into `{}`.`{}` \
                values('{}', '{}', '{}', '{}', '{}', '{}')". \
                    format(database_name, audit_table, batch_id, process_type,
                           sp_name, table_name, input_output, row_count)

                self.sqlContext.sql(sql_stmt)

        except Exception as e:
            print(repr(e))
            print("could not insert the given statment: {}".format(e))
            return False

        return True

    def log_audit_report(self, func):
        @functools.wraps(func)
        def wrapper(*args, **kargs):
            input_tables = kargs
            proc_name = func.__name__
            self.audit_log(table.batch_id, process_type, proc_name, database_name, input_tables, "input")
            values = func(*args, **kargs)
            output_tables = values
            self.audit_log(table.batch_id, process_type, proc_name, database_name, output_tables, "output")

            return values

        return wrapper

    def control_report(self, sqlContext, database_name, batch_id, file_name, file_arrival_time_sftp,
                       last_updated_time_sftp
                       ):
        try:
            control_report_table = 'control_report'

            sql_stmt = "insert into `{}`.`{}` \
            values('{}', '{}', '{}', '{}')". \
                format(database_name, control_report_table, batch_id, file_name, file_arrival_time_sftp,
                       last_updated_time_sftp,  )

            sqlContext.sql(sql_stmt)
        except Exception as e:
            print(repr(e))
            print("could not insert the given statment: {}".format(e))
            return False

        return True

    def log_control_report(self, func):
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            start_time = datetime.datetime.now()
            end_time = None
            proc_name = func.__name__

            self.control_report(table.batch_id, process_type, proc_name, database_name,
                                start_time, end_time)

            value = func(*args, **kwargs)

            end_time = datetime.datetime.now()
            self.control_report(table.batch_id, process_type, proc_name, database_name,
                                start_time, end_time)

            return value

        return wrapper
