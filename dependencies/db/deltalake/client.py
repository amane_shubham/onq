"""

"""
import sys
from cityhash import CityHash64
from dependencies.utils.logger import get_logger
from pyspark.sql.types import StructType, ArrayType, StringType
from pyspark.sql.functions import col, sha2
from pyspark.sql.functions import udf, concat_ws
logging = get_logger()


@udf(returnType=StringType())
def get_city_hash(all_string):
    return CityHash64(all_string)


class DeltaConnector:
    def __init__(self, deltaTable, spark):
        self.deltaTable = deltaTable
        self.spark = spark

    def createDelta(self, dataframe, deltaPath):
        try:
            dataframe = self._delta_transform(dataframe)
            dataframe.write \
            .format("delta") \
            .mode("overwrite") \
            .save(deltaPath)
            return dataframe
        except Exception as error:
            logging.error(f'error in createDelta  {error}')
            raise error.with_traceback(sys.exc_info()[2])

    def readDelta(self, deltaPath):
        try:
            dTable = self.deltaTable.forPath(self.spark, deltaPath)
            return dTable.toDF()
        except Exception as error:
            logging.error(f'error in readDelta  {error}')
            raise error.with_traceback(sys.exc_info()[2])

    def upsertToDelta(self, deltaPath, updateDF):
        try:
            baseDelta = self.deltaTable.forPath(self.spark, deltaPath)
            updateDF = self._delta_transform(updateDF)
            updateDF = updateDF
            baseDelta.alias("t").merge(
                updateDF.alias("s"),
                self._onClause(["orderInformationRecordId"])) \
            .whenMatchedUpdateAll() \
            .whenNotMatchedInsertAll() \
            .execute()
            return baseDelta.toDF()
        except Exception as error:
            logging.error(f'error in upsertToDelta  {error}')
            raise error.with_traceback(sys.exc_info()[2])

    def _onClause(self, keyCols=[]):
        on_clause = " ".join(
                ['t.'+column+'=s.'+column+' AND' for column in keyCols
                ]).rstrip('AND')
        return on_clause

    def _replace_special_characters(self, df):
        for column in df.columns:
            df = df.withColumnRenamed(column, column.replace(" ", "_") \
                                            .replace(".", "_") \
                                            .replace(";", "_") \
                                            .replace(",", "_") \
                                            .replace("-", "_") \
                                            .replace("&", "_") \
                                            .replace("/", "_") \
                                            .replace("(", "_") \
                                            .replace(")", "_"))    
        return df
  
    def _aliasing(self, df, schema):
        replacements = {c: c.replace('.', '__') for c in schema if '.' in c}
        df = df.select([col(c).alias(replacements.get(c, c)) for c in schema])
        return df

    def _cast_array(self, df, schema):
        for col_name, dtype in schema:
            if dtype.startswith('array'):
                df = df.withColumn(col_name, col(col_name).cast('string'))
        return df

    def _flatten(self, schema, prefix=None):
        fields = []
        for field in schema.fields:
            name = prefix + '.' + field.name if prefix else field.name
            dtype = field.dataType
            if isinstance(dtype, ArrayType):
                dtype = dtype.elementType

            if isinstance(dtype, StructType):
                fields += self._flatten(dtype, prefix=name)
            else:
                fields.append(name)

        return fields

    def _rename_duplicate_columns(self, df):
        columns = df.columns
        duplicate_column_indices = list(set([columns.index(col) for col in columns if columns.count(col) == 2]))
        for index in duplicate_column_indices:
            columns[index] = columns[index]+'2'
        df = df.toDF(*columns)
        return df

    def _hash_delta(self, df):
        df = df.withColumn("all_concat", concat_ws("||", *df.columns))
        df = df.withColumn("orderInformationRecordId", get_city_hash("all_concat"))
        # df = df.withColumn("orderInformationRecordId", sha2("all_concat", 256))
        df = df.drop("all_concat")
        return df

    def _delta_transform(self, df):

        df = self._rename_duplicate_columns(df)
        df = self._replace_special_characters(df)
        fields = self._flatten(df.schema)
        df = self._aliasing(df, fields)
        df = self._cast_array(df, df.dtypes)
        df = self._replace_special_characters(df)
        df = self._hash_delta(df)
        return df

