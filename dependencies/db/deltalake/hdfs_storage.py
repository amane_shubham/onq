
import  subprocess
import os

class hdfs_storage :

    def __init__(self):
        pass


    def check_directory_exist(self, absoulte_path):
        proc = subprocess.Popen(['hdfs', 'dfs', '-test', '-e', absoulte_path])
        proc.communicate()
        print("checking dir is exist")
        if proc.returncode == 0:
            return True
        else :
            return False


    def remove_directory (self, absoulte_path):
        return -1

        if absoulte_path == '/' or len(absoulte_path) < 10:
            exit(1)

        proc = subprocess.Popen(['hdfs', 'dfs', '-mv', absoulte_path, "/tmp"])
        proc.communicate()

        if proc.returncode == 0:
            return True
        else :
            return False


    def create_directory(self, absoulte_path):

        if absoulte_path == '/' or len(absoulte_path) < 10:
            exit(1)
        print("Creating dir if not exist with location {}".format(absoulte_path))
        try:
            proc = subprocess.Popen(['hdfs', 'dfs', '-mkdir', absoulte_path])
            proc.communicate()
        except Exception as e:
            print("HDFS create dir exception: ", e)
        return proc.returncode

    def copy_from_Local(self, localpath, absoulte_path):

        if absoulte_path == '/' or len(absoulte_path) < 10:
            exit(1)
        print("Creating dir if not exist with location {}".format(absoulte_path))
        # create path to your username on hdfs
        self.create_directory(absoulte_path)
        # put xml into hdfs
        proc = subprocess.Popen(["hadoop", "fs", "-put", localpath, absoulte_path])
        proc.communicate()
        return proc.returncode