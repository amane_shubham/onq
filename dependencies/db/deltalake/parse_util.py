import xml.etree.ElementTree as ET
import os

path = os.path.dirname(os.path.realpath(__file__))

root_data = []
trans_dict = []
class parseXML:
    def __init__(self):
        pass


    def get_system_data(self, xml_data):
        root_dict = {}
        root_data = []
        for root in xml_data.getroot():
            if len(root.text.strip()) != 0:
                root_dict[root.tag] = root.text.strip()
                if root.tag == 'Hotel_Code':
                    hotel_code = root.text
                elif root.tag == 'Business_Date':
                    buisness_date = root.text.replace("/", "")

        unique_key = hotel_code + '_' + buisness_date
        root_dict['unique_key'] = unique_key
        root_data.append(root_dict)
        return root_data


    merge_list = []


    def merge_two_dict(self, list_dict1, list_dict2):
        merge_list = []
        for d1 in list_dict1:
            for d2 in list_dict2:
                di = {**d1, **d2}
                merge_list.append(di)
                di = {}
        return merge_list


    def get_tag_data(self, search_tag, data):
        try:
            op = data.findall(search_tag)
        except Exception as e:
            print("Exception while parsing file",e)
        return op


    trans_dict = []
    def main_parse(self, current_dict, parent_tag, transactional_tag, skip_tags):
        trans_dict = []
        def parsing(self, current_dict, parent_tag, transactional_tag, skip_tags):
            children = list(parent_tag)

            if (parent_tag.tag == transactional_tag):
                temp_dict = {}
                for child in children:
                    temp_dict[child.tag] = child.text.strip()
                di = {**temp_dict, **current_dict}

                trans_dict.append(di)

            for child in children:
                has_more_children = list(child)

                if len(has_more_children) == 0:
                    if child.tag not in skip_tags:
                        current_dict[child.tag] = child.text.strip()
                else:
                    if child.tag not in skip_tags:
                        self.parsing(current_dict, child, transactional_tag, skip_tags)

            return trans_dict
        data = self.parsing(current_dict, parent_tag, transactional_tag, skip_tags)
        return data

    def req_comment(self, root):
        temp_dict = {}
        t1 = {}
        t2 = []
        list_comments = []
        stay_id = ''
        for parent in root.iter('STAY'):
            for sub_parent in parent:
                if sub_parent.tag == 'stay_id':
                    stay_id = sub_parent.text
                for sub_parent in sub_parent.iter('REQUESTS_COMMENTS'):
                    t1 = {}
                    for child in sub_parent:
                        if 'special_request_desc' == child.tag:
                            temp_dict = {}
                            temp_dict[child.tag] = child.text
                        elif "special_request_text" == child.tag:
                            temp_dict[child.tag] = child.text
                            temp_dict['stay_id'] = stay_id

                            list_comments.append(temp_dict)

        return list_comments

    def column_name_list_fom_df(schema_array):
        lm_column = []
        pm_column = []
        am_column = []
        for py in schema_array:
            if py['ONQ_RAW_Location_Metrics'] == 'Y':
                lm_column.append(py['data_point_Name_in_OnQ'])
            if py['ONQ_RAW_Agent_Metrics'] == 'Y':
                am_column.append(py['data_point_Name_in_OnQ'])
            if py['ONQ_RAW_Product_Metrics'] == 'Y':
                pm_column.append(py['data_point_Name_in_OnQ'])
        return am_column, lm_column, pm_column