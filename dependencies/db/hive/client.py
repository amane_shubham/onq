"""

"""
import sys

from pyspark.sql.functions import lit

from dependencies.utils.logger import get_logger
from table import table

logging = get_logger()


class hive_client:

    def __init__(self, spark):
        self.spark = spark

    def drop_column_if_exists(self, dataframe, column_name):

        if column_name in dataframe.columns:
            dataframe = dataframe.drop(column_name)

        return dataframe

    def add_column_if_not_exist(self, dataframe, column_name, data_type='string'):
        if not column_name in dataframe.columns:
            dataframe = dataframe.withColumn(column_name, lit(table.batch_id).cast(data_type))
        return dataframe

    def read_table(self, sqlContext, db_name, dbTable, skip_batch_id=False):
        try:
            dataframe = sqlContext.table(db_name + "." + dbTable)

            if skip_batch_id == False:
                dataframe = self.drop_column_if_exists(dataframe, "batch_id")

            return dataframe
        except Exception as error:
            logging.error('error in read_hive_table  {error}'.format(error=error))
            raise error.with_traceback(sys.exc_info()[2])

    def read_query(self, sqlContext, query, skip_batch_id=False):
        try:
            dataframe = sqlContext.table(query)

            if skip_batch_id == False:
                dataframe = self.drop_column_if_exists(dataframe, "batch_id")

            return dataframe


        except Exception as error:
            logging.error('error in read_hive_table  {error}'.format(error=error))
            raise error.with_traceback(sys.exc_info()[2])

    def write_dataframe_as_table_overwrite(self, spark, dataframe, db_name, dbTable, absolute_path, mode='overwrite',
                                           skip_batch_id=False):
        try:
            if skip_batch_id == False:
                dataframe = self.add_column_if_not_exist(dataframe, "batch_id")
            temp_path = str(table.temp_path) + "_" + dbTable
            print(temp_path)
            logging.info("temp_path for tabe {} path {}".format(dbTable, temp_path))

            dataframe.write.mode(mode).parquet(temp_path)
            dataframe = spark.read.load(temp_path)
            dataframe.write.mode(mode).option(
                "path", absolute_path).format("parquet").saveAsTable(db_name + "." + dbTable)

            print("writing into " + absolute_path)

        except Exception as error:
            logging.error('error in write_dataframe  {error}'.format(error=error))
            raise error.with_traceback(sys.exc_info()[2])

    def write_dataframe_as_append(self, dataframe, abosulte_path, mode='append', skip_batch_id=False):
        try:
            if skip_batch_id == False:
                dataframe = self.add_column_if_not_exist(dataframe, "batch_id")

            dataframe.write.mode(mode).parquet(abosulte_path)

        except Exception as error:
            logging.error('error in write_dataframe  {error}'.format(error=error))
            raise error.with_traceback(sys.exc_info()[2])

    def _read_dataframe_with_merge(self, sc, path, skip_batch_id=False):

        dataframe = sc.read.option("mergeSchema", True).load(path)

        if skip_batch_id == False:
            dataframe = self.drop_column_if_exists(dataframe, "batch_id")

        return dataframe

    def write_dataframe_as_directory_overwrite(self, dataframe, abosulte_path, skip_batch_id=False):

        try:
            if skip_batch_id == False:
                dataframe = self.add_column_if_not_exist(dataframe, "batch_id")

            dataframe.write.mode('overwrite').parquet(abosulte_path)
            print ("writing into "+abosulte_path)

        except Exception as error:
            logging.error('error in write_dataframe  {error}'.format(error=error))
            raise error.with_traceback(sys.exc_info()[2])

    def refresh_table(self, sqlContext, db_name, dbTable):
        query = "REFRESH TABLE {db}.{table}".format(db=db_name,table=dbTable)
        sqlContext.sql(query)
        return None