"""
ingauge_ingest.py
~~~~~~~~~~
"""
import errno
import os
import pandas as pd
from dynaconf import settings
import stat
from dependencies.connector.sftp_connector import SftpConnect
from dependencies.db.deltalake.hdfs_storage import hdfs_storage
from table import table
import re
import json
path = os.path.dirname(os.path.realpath(__file__))


path = os.path.dirname(os.path.realpath(__file__))
ENV = path + "/ingest_conf.json"
config = json.load(open(ENV))
sftp_con = SftpConnect()
sftp_connections = sftp_con.connect_to_sftp(sftp_host=settings.SFTP_HOST,
                                       sftp_port=int(settings.SFTP_PORT),
                                       sftp_username=settings.SFTP_USERNAME,
                                       sftp_key= settings.SFTP_KEY_PATH+settings.SFTP_KEY_NAME)

print(sftp_connections)
sink = hdfs_storage()
# force_full_refresh = False
read_path_name = config['API']['ONQ']['READ_PATH']
sink_db_name = settings.HV_ONQ_SINK_DATABASE_NAME
table.backup_location = config['API']['ONQ']['API_ONQ_BACKUP_DIRECTORY_PATH']
row_tag = config['API']['ONQ']['rowTag']
temp_path = config['API']['ONQ']['temp_path']

# Switch to a remote directory
sftp_connections.chdir(read_path_name)
# Obtain structure of the remote directory 'path
directory_structure = sftp_connections.listdir_attr()
listdir = sftp_connections.listdir(read_path_name)
file_name_path = {}
fileattrib = []
file_list = [{'dir_name': 'auhamhi',
  'file_name': 'fpg_auhamhi021119.xml',
  'file_path': '/efs/pulse-home-3/hilton-ftp-backup/11042019/fpg_auhamhi021119.xml'},
 {'dir_name': 'snaaoes',
  'file_name': 'fpg_snaaoes022119.xml',
  'file_path': '/efs/pulse-home-3/hilton-ftp-backup/11042019/fpg_snaaoes022119.xml'},
 {'dir_name': 'rlscdt',
  'file_name': 'fpg_rlscdt021619.xml',
  'file_path': '/efs/pulse-home-3/hilton-ftp-backup/11042019/fpg_rlscdt021619.xml'},
]
latest_timestamp = 0
def get_sftp_file_list(path, latest = 0):
    file_name_path = {}
    fileattrib = []
    def get_file_names(path, latest= 0):
    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        for fileattr in sftp_connections.listdir_attr(path) :
            if stat.S_ISDIR(fileattr.st_mode) and fileattr.st_mtime > latest:
                print(fileattr.filename, "::", os.path.join(path, fileattr.filename))
                latest = fileattr.st_mtime
                get_file_names(os.path.join(path, fileattr.filename), latest)
            elif stat.S_ISREG(fileattr.st_mode):

                location_name = fileattr.filename.split("_")[1].split(".")[0]
                dir_name = " ".join(re.findall("[a-zA-Z]+", location_name))

                file_name_path['dir_name'] = dir_name
                file_name_path['file_name'] = fileattr.filename
                file_name_path['file_path'] = os.path.join(path, fileattr.filename)
                file_name_path['file_arrival_time'] = fileattr.st_mtime

                fileattrib.append(file_name_path)
                file_name_path = {}
        return fileattrib
    sftp_data = get_file_names(path, latest)
    return sftp_data
# get_sftp_file_list(read_path_name)

def read_from_sftp_store_to_hdfs(file_list):
    for i in file_list:
        print(i['dir_name'])
        print(i['file_name'])
        print(i['file_path'])
        print('\n')

        path = temp_path+i['dir_name']+"/"
        if not os.path.exists(path):
            try:
                os.makedirs(path)
            except OSError as e:
                if e.errno != errno.EEXIST:
                    raise

        sftp_connections.get(i['file_path'],path+i['file_name'])
        hdfs_abs_path = table.backup_location+i['dir_name']+"/"
        sink.copy_from_Local(path+i['file_name'],hdfs_abs_path)
        print("File uploaded successfully")

# read_from_sftp_store_to_hdfs(file_list)