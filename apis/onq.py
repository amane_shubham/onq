"""
ingauge_ingest.py
~~~~~~~~~~
"""
import os
import errno
import stat
import pandas as pd
from dynaconf import settings
from dependencies.utils.logger import get_log_file
from dependencies.utils.spark import start_spark
from dependencies.db.deltalake.delta_connector import delta_connector
from dependencies.db.hive.client import hive_client
from pyspark.sql import HiveContext, SQLContext, SparkSession
from pyspark.sql.functions import col, regexp_replace, lower, lit, unix_timestamp, from_unixtime
from dependencies.db.deltalake.parse_util import parseXML
import xml.etree.ElementTree as ET
import pprint
from datetime import datetime
import json
import sys
import re
from dependencies.connector.sftp_connector import SftpConnect
from dependencies.db.deltalake.hdfs_storage import hdfs_storage
from table import table
import json

path = os.path.dirname(os.path.realpath(__file__))

path = os.path.dirname(os.path.realpath(__file__))
ENV = path + "/ingest_conf.json"
config = json.load(open(ENV))

spark, log, config, batch_id = start_spark(
    app_name='ONQ_job',
    files=[path + '/ingest_conf.json'],
    jar_packages=['com.crealytics:spark-excel_2.11:0.12.4'])

sc = spark.sparkContext

sqlContext = HiveContext(spark)

sink = hdfs_storage()
client = hive_client(spark)
force_full_refresh = True
sink_connector = delta_connector(spark, client)

sftp_con = SftpConnect()
sftp_connections = sftp_con.connect_to_sftp(sftp_host=settings.SFTP_HOST,
                                       sftp_port=int(settings.SFTP_PORT),
                                       sftp_username=settings.SFTP_USERNAME,
                                       sftp_key= settings.SFTP_KEY_PATH+settings.SFTP_KEY_NAME)

print(sftp_connections)
pxml = parseXML()
# force_full_refresh = False
excel_file_path = 'Users/shubhamamane/Downloads/'
excel_file_name = path + '/../onq_datapoints.xlsx'

read_path_name = config['API']['ONQ']['READ_PATH']
table.sink_db_name = settings.HV_ONQ_SINK_DATABASE_NAME
table.backup_location = config['API']['ONQ']['API_ONQ_BACKUP_DIRECTORY_PATH']
sink.create_directory(table.backup_location)
row_tag = config['API']['ONQ']['rowTag']
temp_path = config['API']['ONQ']['temp_path']
table.batch_id = batch_id

# Switch to a remote directory
sftp_connections.chdir(read_path_name)
## Obtain structure of the remote directory 'path
directory_structure = sftp_connections.listdir_attr()
listdir = sftp_connections.listdir(read_path_name)
file_name_path = {}
fileattrib = []
# file_list = [{'dir_name': 'auhamhi',
#   'file_name': 'fpg_auhamhi021119.xml',
#   'file_path': '/efs/pulse-home-3/hilton-ftp-backup/11042019/fpg_auhamhi021119.xml',
#   'file_arrival_time': 1549938034},
#  {'dir_name': 'snaaoes',
#   'file_name': 'fpg_snaaoes022119.xml',
#   'file_path': '/efs/pulse-home-3/hilton-ftp-backup/11042019/fpg_snaaoes022119.xml',
#   'file_arrival_time': 1550842196},
#  {'dir_name': 'rlscdt',
#   'file_name': 'fpg_rlscdt021619.xml',
#   'file_path': '/efs/pulse-home-3/hilton-ftp-backup/11042019/fpg_rlscdt021619.xml',
#   'file_arrival_time': 1550405477},]

def set_param(table_name):
    table.table_name = table_name
    table.parent_directory_absolute_path_raw = config['API']['ONQ']['API_ONQ_RAW_DIRECTORY_PATH']
    table.parent_directory_absolute_path_history = config['API']['ONQ']['API_ONQ_HISTORY_DIRECTORY_PATH']
    table.raw_table_absolute_path = table.parent_directory_absolute_path_raw + table.table_name
    table.history_table_absolute_path = table.parent_directory_absolute_path_history + table.table_name
    table.batch_id = batch_id

    table.raw_table_name = table.table_name
    table.history_table_name = table.table_name + "_history"
    table.temp_path = config['API']['ONQ']['temp_dir_path']
    table.last_modified_timestamp_column = config['API']['ONQ']['last_modified_timestamp_column']
    # table.created_at_timestamp_column = control_table_row['created_at_timestamp_column']
    # table.unqiue_identfier = control_table_row['unique_identfier'].split(",")


def main_parse(current_dict, parent_tag, transactional_tag, skip_tags):
    trans_dict = []
    def parsing(current_dict, parent_tag, transactional_tag, skip_tags):
        children = list(parent_tag)
        di = {}
        if (parent_tag.tag == transactional_tag):
            temp_dict = {}
            for child in children:
                temp_dict[child.tag] = child.text
            di = {**temp_dict, **current_dict}

            trans_dict.append(di)
            di = {}

        for child in children:
            has_more_children = list(child)

            if len(has_more_children) == 0:
                if child.tag not in skip_tags:
                    current_dict[child.tag] = child.text
            else:
                if child.tag not in skip_tags:
                    parsing(current_dict, child, transactional_tag, skip_tags)
        return trans_dict

    data = parsing(current_dict, parent_tag, transactional_tag, skip_tags)
    return data


def parse_xml_from_file(xmlPath):
    try:
        tree = ET.parse(xmlPath)
        root = tree.getroot()
    except Exception as e:
        log.info("Parse Error : " + e)
        print("Parse Error : " + e)
    return tree


def get_pm_data(tree):
    # try:
    system21_data = pxml.get_system_data(tree)
    stay_data = pxml.get_tag_data('STAY', tree)
    if len(stay_data) != 0:
        pms_data = []
        for stays in stay_data:
            pm_data = main_parse(current_dict={}, parent_tag=stays, transactional_tag="TRANS",
                                 skip_tags=["REQUESTS_COMMENTS"])
            pms_data.append(pm_data)

        stay_pm_data = [item for sublist in pms_data for item in sublist]
        main_pm = pxml.merge_two_dict(system21_data, stay_pm_data)
    else:
        main_pm = system21_data

    return main_pm


def get_pm_request_comment(root):
    try:
        req_comment = pxml.req_comment(root)
    except Exception as e:
        log.info("Error : " + e)
        print("Error : " + e)
    if len(req_comment) !=0:
        print("req_comment ",len(req_comment))
        return req_comment


def get_lm_data(tree):
    try:
        system21_data = pxml.get_system_data(tree)
        stat_data = pxml.get_tag_data('STAT', tree)
        if len(stat_data) != 0:
            for stat in stat_data:
                lm_data = main_parse(current_dict={}, parent_tag=stat, transactional_tag="TRAVEL_REASON",
                                     skip_tags=["EMPLOYEE"])
                main_lm = pxml.merge_two_dict(system21_data, lm_data)
        else:
            main_lm = system21_data

    except Exception as e:
        log.info("Error : " + e)
        print("Error : " + e)
    return main_lm


def get_am_data(tree):
    try:

        system21_data = pxml.get_system_data(tree)
        stat_data = pxml.get_tag_data('STAT', tree)
        if len(stat_data) != 0:
            for stat in stat_data:
                am_data = main_parse({}, parent_tag=stat, transactional_tag="EMPLOYEE",
                                     skip_tags=["TRAVEL_REASON", "total_arrivals", "total_room_revenue",
                                                "total_occupied"])
                main_am = pxml.merge_two_dict(system21_data, am_data)
        else:
            main_am = system21_data

    except Exception as e:
        log.info("Error : " + e)
        print("Error : " + e)
    return main_am


def make_df_from_api(api_data):
    try:
        json_rdd = sc.parallelize(api_data).map(lambda x: json.dumps(x))
        df = spark.read.json(json_rdd)
    except Exception as e:
        log.info("Error : " + e)
        print("Error : " + e)
    return df


def load_data(table_name, df):
    """Loading API dataframe as table in Snowflake
    :return: None
    """
    set_param(table_name)
    table_raw_directory = table.raw_table_absolute_path
    table_history_directory = table.history_table_absolute_path
    history_table_name = table.history_table_name
    raw_table_name = table.raw_table_name
    sink_db_name = table.sink_db_name
    last_modified_time_column = table.last_modified_timestamp_column
    unqiue_identfier = table.unqiue_identfier
    new_dataframe = sink_connector._delta_transform(df)
    is_incremental = table.is_incremental

    if sink.check_directory_exist(table_raw_directory):

        raw_table_dataframe = client.read_table(sqlContext, sink_db_name, raw_table_name)

        if sink_connector.is_schema_same(raw_table_dataframe, new_dataframe):

            if is_incremental == True and unqiue_identfier != None and last_modified_time_column != None:

                last_run_max_timestamp = raw_table_dataframe.selectExpr(
                    "cast(max({0}) as timestamp) as {0}".format(last_modified_time_column)).collect()[
                    0].__getitem__(last_modified_time_column)

                new_dataframe = new_dataframe.filter(col(last_modified_time_column) > last_run_max_timestamp)
                log.info("Incremental load found {} table".format(table_name))
                print("Incremental load found {} table".format(table_name))
                sink_connector.insert_incremental_data(new_dataframe, raw_table_dataframe)

            else:

                sink_connector.insert_full_load_data(new_dataframe, raw_table_dataframe)
        else:
            log.info("Schema change executions start {} table".format(table_name))
            print("Schema change executions start {} table".format(table_name))
            sink_connector.insert_data_on_scehma_change(new_dataframe, raw_table_dataframe)

        return None

    elif (not sink.check_directory_exist(table_raw_directory)) :
        log.info("Full run start {} table".format(table_name))
        print("Full run start {} table".format(table_name))
        sink_connector.insert_first_load(new_dataframe)
        log.info("Full run finished {} table".format(table_name))
    else:
        log.warn("Need to clean up HDFS directory, drop the Hive tables and re-run the scripts")
        print("Need to clean up HDFS directory, drop the Hive tables and re-run the scripts")


def load_data_from_xml(tree):
    status_dict = {'Table': [], 'Status': [], 'Comments': []}
    schema_df = sink_connector.read_excel(spark, excel_file_name)
    schema_df.cache()
    schema_array = schema_df.groupBy(schema_df.extract_nection_name_in_ONQ, schema_df.data_point_Name_in_OnQ,
                                     schema_df.ONQ_RAW_Location_Metrics, schema_df.ONQ_RAW_Agent_Metrics,
                                     schema_df.ONQ_RAW_Product_Metrics,
                                     schema_df.data_point_name_in_Datalake).count().orderBy(
        schema_df.extract_nection_name_in_ONQ, schema_df.data_point_Name_in_OnQ,
        schema_df.ONQ_RAW_Location_Metrics, schema_df.ONQ_RAW_Agent_Metrics,
        schema_df.ONQ_RAW_Product_Metrics, schema_df.data_point_name_in_Datalake).collect()
    am_lists, lm_lists, pm_lists = parseXML.column_name_list_fom_df(schema_array)


    pm_req_comment_df = 0
    try:
        pm = get_pm_data(tree)
        pm_df = make_df_from_api(pm)
        pm_df = sink_connector.add_column_not_exist(pm_df, pm_lists)
    except Exception as e:
        print("Exception at pm block :",e)
    # pm_df = sink_connector.custom_schema(schema_array, pm_df, table_name="ONQ_RAW_Product_Metrics")

    try:
        lm = get_lm_data(tree)
        lm_df = make_df_from_api(lm)
        lm_df = sink_connector.add_column_not_exist(lm_df, lm_lists)
    except Exception as e:
        print("Exception at lm block ", e)
    # lm_df = sink_connector.custom_schema(schema_array, lm_df, table_name="ONQ_RAW_Location_Metrics")
    try:
        am = get_am_data(tree)
        am_df = make_df_from_api(am)
        am_df = sink_connector.add_column_not_exist(am_df, am_lists)
    except Exception as e:
        print("Exception at am block ", e)
    # am_df = sink_connector.custom_schema(schema_array, am_df, table_name="ONQ_RAW_Agent_Metrics")


    try:
        root = tree.getroot()
        pm_req_comment = get_pm_request_comment(root)
        if pm_req_comment != None or len(pm_req_comment) != 0:
            pm_req_comment_df = make_df_from_api(pm_req_comment)
            pm_req_comment_df = sink_connector.add_column_not_exist(pm_req_comment_df,
                                                                    ['special_request_desc',
                                                                     'special_request_text'])
        #     columns = ['special_request_desc','special_request_text']
        #     for i in pm_req_comment_df.columns:
        #         if i == 'special_request_desc' :
        #             pm_req_comment_df = pm_req_comment_df.withColumn('special_request_description', pm_req_comment_df[str(i)].cast("string"))  # .cast(py['hive_datatype']))
        #         elif i == 'special_request_text':
        #             pm_req_comment_df = pm_req_comment_df.withColumn('special_request_notes', pm_req_comment_df[str(i)].cast("string") )
        # pm_req_comment_df = pm_req_comment_df
    except Exception as e:
        print("exception at request comment :", e)

    if (table.is_active):
        try:
            table.table_name = "ONQ_RAW_Product_Metrics"
            load_data(table.table_name, pm_df)
            client.refresh_table(sqlContext,table.sink_db_name, table.table_name)

            table.table_name = "ONQ_RAW_Location_Metrics"
            load_data(table.table_name, lm_df)
            client.refresh_table(sqlContext, table.sink_db_name, table.table_name)

            table.table_name = "ONQ_RAW_Agent_Metrics"
            load_data(table.table_name, am_df)
            client.refresh_table(sqlContext, table.sink_db_name, table.table_name)

            if pm_req_comment != None or int(pm_req_comment_df.count()) !=0:
                table.table_name = "request_comment"
                load_data(table.table_name, pm_req_comment_df)
                client.refresh_table(sqlContext, table.sink_db_name, table.table_name)
            # status_dict['Table'].append(table.table_name)
            # status_dict['Status'].append('Completed')
            # status_dict['Comments'].append('Loaded Successfully')
            # log.info('Table was loaded: {}'.format(table.table_name))
            # print('Table was loaded: {}'.format(table.table_name))
        except Exception as e:
            log.error(
                "Failed loading in dataframe: {}".format(str(e)))
            # status_dict['Table'].append(table.table_name)
            # status_dict['Status'].append('Failed')
            # status_dict['Comments'].append(str(e))
            # # log.error('Table failed loading: {}'.format(table.table_name))
    return status_dict


file_name_path = {}
fileattrib = []
def get_file_names(path, latest= 0):
    file_name_path = {}
# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    for fileattr in sftp_connections.listdir_attr(path) :
        if stat.S_ISDIR(fileattr.st_mode) and fileattr.st_mtime > latest:
            print(fileattr.filename, "::", os.path.join(path, fileattr.filename))
            latest = fileattr.st_mtime
            get_file_names(os.path.join(path, fileattr.filename), latest)
        elif stat.S_ISREG(fileattr.st_mode):

            location_name = fileattr.filename.split("_")[1].split(".")[0]
            dir_name = " ".join(re.findall("[a-zA-Z]+", location_name))

            file_name_path['dir_name'] = dir_name
            file_name_path['file_name'] = fileattr.filename
            file_name_path['file_path'] = os.path.join(path, fileattr.filename)
            file_name_path['file_arrival_time'] = fileattr.st_mtime

            fileattrib.append(file_name_path)
            file_name_path = {}
    return fileattrib

def read_from_sftp_store_to_hdfs(file_list):
    for i in file_list:
        print(i['dir_name'])
        print(i['file_name'])
        print(i['file_path'])
        print(i['file_arrival_time'])
        file_arrival_time = datetime.fromtimestamp(i['file_arrival_time']).strftime('%Y-%m-%d %H:%M:%S')
        last_updated_time_sftp = datetime.utcnow()
        # last_updated_time_sftp_timestamp = datetime.timestamp(last_updated_time_sftp)
        print('\n')
        #
        path = temp_path+i['dir_name']+"/"
        if not os.path.exists(path):
            try:
                os.makedirs(path)
            except OSError as e:
                if e.errno != errno.EEXIST:
                    raise

        sftp_connections.get(i['file_path'],path+i['file_name'])
        hdfs_abs_path = table.backup_location+i['dir_name']+"/"
        #
        print("Parse xml path from local", path+i['file_name'])
        # tree_data = parse_xml_from_file(path + i['file_name'])
        xmlfile_path = path + i['file_name']
        tree = ET.parse(xmlfile_path)

        # tree_data = parse_xml_from_file('/tmp/hilton_data/auhamhi/fpg_auhamhi021119.xml')
        print("started the processing file : ", i['file_name'])
        load_data_from_xml(tree)
        print("Processing completed of :", i['file_name'])

        sink.copy_from_Local(path+i['file_name'],hdfs_abs_path)
        sink_connector.control_report(sqlContext,table.sink_db_name, table.batch_id,i['file_name'],
                                      file_arrival_time, last_updated_time_sftp,
                                      )
        print("File uploaded successfully")

def onq():
    log.info('Getting last loaded timestamp: {}'.format("control_report"))
    max_time_stamp = sqlContext.table('{db_name}.{table}'.format(db_name=table.sink_db_name, table='control_report'))
    # last_upated_timestamp = max_time_stamp.selectExpr("max(last_updated_time_sftp) as last_updated_time_sftp").select(
    #     unix_timestamp('last_updated_time_sftp', 'yyyyMMddHHmmss').alias("last_updated")).collect()[0].__getitem__(
    #     "last_updated")

    last_upated_timestamp = max_time_stamp.selectExpr("max(file_arrival_time_sftp) as file_arrival_time_sftp").select(
        unix_timestamp('file_arrival_time_sftp', 'yyyyMMddHHmmss').alias("last_updated")).collect()[0].__getitem__(
        "last_updated")

    log.info('Getting last loaded timestamp: {}'.format(last_upated_timestamp))
    log.info('Getting files from sftp location')
    file_list = get_file_names(read_path_name, latest= last_upated_timestamp)
    print("sftp file list : ",len(file_list))
    log.info('Reading sftp files from sftp location')
    read_from_sftp_store_to_hdfs(file_list)
    log.info('Process completed')

onq()